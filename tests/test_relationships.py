from typing import List

from sqlalchemy_orm.database import Database

from sqlalchemy_orm.base.base import Model


class TestModel:
    def test_one_to_one(self):
        Base = Model()

        class Person(Base):
            name: str
            ni_number: "NINumber"

        class NINumber(Base):
            number: str

        db = Database("sqlite:///:memory:")
        db.create(Person)

        session = db.session()

        rob = Person(name="Rob", ni_number=NINumber(number="1234"))

        session.create(rob)

    def test_one_to_many(self):
        Base = Model()

        class Pet(Base):
            name: str

        class PetOwner(Base):
            name: str
            pets: List[Pet] = None

        db = Database("sqlite:///:memory:")
        db.create(Pet, PetOwner)

        session = db.session()

        pet_owner = PetOwner(name="molly", pets=[])

        pet_1 = Pet(name="bea")
        pet_2 = Pet(name="chloe")

        pet_owner.pets.append(pet_1)
        pet_owner.pets.append(pet_2)

        session.create(pet_owner, pet_1, pet_2)

    def test_many_to_one(self):
        Base = Model()

        class Restaurant(Base):
            name: str

        class Woman(Base):
            id: str
            favourite_restaurant: Restaurant

        restaurant = Restaurant(name="pizza")

        db = Database("sqlite:///:memory:")
        db.create(Restaurant, Woman)
        session = db.session()

        woman_1 = Woman(id=1, favourite_restaurant=restaurant)
        woman_2 = Woman(id=2, favourite_restaurant=restaurant)

        session.create(woman_1, woman_2)

    def test_many_to_many_association(self):
        Base = Model()

        class Customer(Base):
            name: str
            orders: List["Order"]  # noqa: F821

        class Order(Base):
            order_no: int
            customer: Customer
            product_orders: List["ProductOrder"]  # noqa: F821

        class Product(Base):
            product_no: int
            product_orders: List["ProductOrder"]  # noqa: F821

            @property
            def orders(self):
                return [product_order.order for product_order in self.product_orders]

        class ProductOrder(Base):
            id: int
            product: Product
            order: Order

        db = Database("sqlite:///:memory:")
        db.create(Customer, Order, Product)
        session = db.session()

        customer = Customer(name="molly")

        product_1 = Product(product_no=1)
        product_2 = Product(product_no=2)
        product_3 = Product(product_no=3)

        session.create(customer, product_1, product_2, product_3)

        order_1 = Order(order_no=1, customer=customer)
        product_order_1 = ProductOrder(id=1, order=order_1, product=product_1)
        product_order_2 = ProductOrder(id=2, order=order_1, product=product_2)

        order_2 = Order(order_no=2, customer=customer)
        product_order_3 = ProductOrder(id=3, order=order_2, product=product_2)
        product_order_4 = ProductOrder(id=4, order=order_2, product=product_3)

        session.create(order_1, product_order_1, product_order_2)
        session.create(order_2, product_order_3, product_order_4)

        products = session.query(Product).all()

        assert products[0].orders == [order_1]
        assert products[1].orders == [order_1, order_2]
        assert products[2].orders == [order_2]

        assert customer.orders == [order_1, order_2]

    def test_many_to_one_filter(self):
        Base = Model()

        class Country(Base):
            name: str
            population: int
            timezone: "TimeZone"

        class TimeZone(Base):
            name: str
            offset_in_hours: int
            countries: List[Country]

        db = Database("sqlite:///:memory:")

        db.create(Country)
        db.create(TimeZone)

        session = db.session()

        countries = [
            Country(name="England", population=50000000),
            Country(name="America", population=300000000),
            Country(name="Mexico", population=62000000),
        ]

        for country in countries:
            session.add(country)

        eastern_time = TimeZone(
            name="EST", offset_in_hours="-5", countries=[countries[1], countries[2]]
        )
        session.add(eastern_time)

        countries = (
            session.query(Country).filter(Country.timezone.has(name="EST")).all()
        )
        countries_names = [country.name for country in countries]

        assert countries_names == ["America", "Mexico"]
