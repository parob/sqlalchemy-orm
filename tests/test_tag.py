import pytest

from sqlalchemy_orm import Model, UUIDType, TypeMapper
from sqlalchemy_orm.database import Database
from sqlalchemy_orm.patterns.tag import TagBase
from sqlalchemy_orm.patterns.tag_mixin import TagMixin


class TestModel:
    @pytest.mark.skip
    def test_db_tag(self):
        Base = Model(type_map=TypeMapper(types=[UUIDType]))

        class Animal(TagMixin, Base):
            name: str
            age: int

        db = Database("sqlite:///:memory:")
        db.create(Animal)

        animal = Animal(name="Chloe", age=11)
        animal.tags.append("cat")

        animal2 = Animal(name="Chloe2", age=11)
        animal2.tags.append("cat")

        assert "cat" in animal.tags
        assert Animal in db

        session = db.session()
        session.create(animal, animal2)
        session.flush()

        chloe = Animal(name="Chloe", age=15)
        chloe.tags.append("cat")
        chloe.tags.append("brown")

        bea = Animal(name="Bea", age=11)
        bea.tags.append("dog")
        bea.tags.append("schnauzer")

        molly = Animal(name="Molly", age=13)
        molly.tags.append("dog")
        molly.tags.append("black_dog")
        molly.tags.append("age_13")

        assert "cat" in chloe.tags
        assert Animal in db

        session = db.session()
        session.create(chloe, bea, molly)
        session.flush()

        assert chloe in session
        assert bea in session
        assert molly in session

        dogs = session.query(Animal).filter(Animal.tags.contains("dog")).all()

        assert dogs == [bea, molly]

    @pytest.mark.skip
    def test_tag(self):
        Base = Model(type_map=TypeMapper(types=[UUIDType]))

        class Tag(TagBase, Base):
            pass

        TagMixin = Tag.mixin()

        class Animal(TagMixin, Base):
            name: str
            age: int

        db = Database("sqlite:///:memory:")
        db.create(Animal, Tag)

        chloe = Animal(name="Chloe", age=15)
        chloe.tags.append("cat")
        chloe.tags.append("brown")

        bea = Animal(name="Bea", age=11)
        bea.tags.append("dog")
        bea.tags.append("schnauzer")

        molly = Animal(name="Molly", age=13)
        molly.tags.append("dog")
        molly.tags.append("black_dog")
        molly.tags.append("age_13")

        assert "cat" in chloe.tags
        assert Animal in db

        session = db.session()
        session.create(chloe, bea, molly)
        session.flush()

        assert chloe in session
        assert bea in session
        assert molly in session

        dogs = (
            session.query(Animal)
            .filter(Animal.tag_associations.any(Tag.tag == "dog"))
            .all()
        )

        assert dogs == [bea, molly]
