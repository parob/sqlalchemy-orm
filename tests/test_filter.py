from typing import List

from sqlalchemy_orm import Model, Database
from sqlalchemy_orm.filter import Filter


class TestFilter:
    def test_dataclass(self):
        Base = Model()

        class Animal(Base):
            name: str
            age: int
            size: int

        db = Database("sqlite:///:memory:")

        db.create(Animal)

        class AnimalFilter(Filter):
            def __init__(
                self,
                name: str = None,
                age: str = None,
                or_: List["AnimalFilter"] = None,  # noqa: F821
                and_: List["AnimalFilter"] = None,  # noqa: F821
            ):
                super().__init__(or_=or_, and_=and_)
                self.name = name
                self.age = age

            def criterion(self, entities=None):
                criterion = super().criterion(entities=entities)

                if self.name:
                    criterion.append(Animal.name.ilike(f"%{self.name}%"))

                if self.age:
                    criterion.append(Animal.age == self.age)

                return criterion

        animal = Animal(name="baxter", age="10", size=10)

        session = db.session()

        session.create(animal)

        assert animal in session
        assert session.query(Animal).filter(AnimalFilter(name="baxter")).one() == animal
        assert session.query(Animal).filter(AnimalFilter(age="10")).one() == animal

        assert (
            session.query(Animal)
            .filter(
                AnimalFilter(or_=[AnimalFilter(name="tom"), AnimalFilter(age="10")])
            )
            .one_or_none()
            == animal
        )

        assert (
            not session.query(Animal)
            .filter(
                AnimalFilter(and_=[AnimalFilter(name="tom"), AnimalFilter(age="10")])
            )
            .one_or_none()
        )
