import uuid
from typing import List, Dict
from uuid import UUID

import pytest
from sqlalchemy import text, Column, JSON
from sqlalchemy.orm import mapped_column

from sqlalchemy_orm import TypeMapper, EnumType, UUIDType

import enum
from sqlalchemy_orm.base.base import Model
from sqlalchemy_orm.database import Database


def is_pgvector_installed():
    try:
        from pgvector.sqlalchemy import Vector

        assert Vector
    except ImportError:
        return False

    return True


class TestTypes:
    def test_string(self):
        Base = Model()

        class File(Base):
            data: str

        db = Database("sqlite:///:memory:")

        db.create(File)
        assert db.validate(Base)

        session = db.session()
        data = "Test Data" * 500
        file = File(data=data)

        session.create(file)
        session.commit()

        loaded_file: File = session.query(File).filter(File.data == data).one()

        assert loaded_file

    def test_sqlalchemy_list(self):
        Base = Model()

        class File(Base):
            id: UUID = uuid.uuid4
            list_of_items = Column(JSON())

        db = Database("sqlite:///:memory:")

        db.create(File)
        assert db.validate(Base)

        session = db.session()
        data = ["Data1", "Data2"]
        file = File(list_of_items=data)

        session.create(file)
        session.commit()

        loaded_file: File = session.query(File).filter(File.list_of_items == data).one()

        assert loaded_file

    def test_list(self):
        Base = Model()

        class File(Base):
            id: UUID = uuid.uuid4
            data: List[str]
            permissions: List[float]

        db = Database("sqlite:///:memory:")

        db.create(File)
        assert db.validate(Base)

        session = db.session()
        data = ["Data1", "Data2"]
        permissions = [4.1, 3.4, 6]
        file = File(data=data, permissions=permissions)

        session.create(file)
        session.commit()

        loaded_file: File = session.query(File).filter(File.data == data).one()

        assert loaded_file

    def test_dict(self):
        Base = Model()

        class File(Base):
            id: UUID = uuid.uuid4
            data: Dict[str, str]

        db = Database("sqlite:///:memory:")

        db.create(File)
        assert db.validate(Base)

        session = db.session()
        data = {"name": "file_name", "data": "value1"}
        file = File(data=data)

        session.create(file)
        session.commit()

        loaded_file: File = session.query(File).filter(File.data == data).one()

        assert loaded_file

    def test_bytes(self):
        Base = Model()

        class File(Base):
            name: str
            data: bytes

        db = Database("sqlite:///:memory:")

        db.create(File)
        assert db.validate(Base)

        session = db.session()
        name = "test_file"
        data = b"hello world"
        file = File(name=name, data=data)

        session.create(file)
        session.commit()

        assert file.data == session.query(File).filter(File.name == name).one().data
        assert file.name == session.query(File).filter(File.data == data).one().name

    def test_enum(self):
        class BuildingType(enum.Enum):
            house = "house"
            flat = "flat"
            shop = "shop"

        Base = Model(type_map=TypeMapper(types=[EnumType]))

        class Building(Base):
            name: str
            type: BuildingType

        db = Database("sqlite:///:memory:")

        db.create(Building)
        session = db.session()

        building = Building(name="home", type=BuildingType.house)
        session.create(building)

        building.type = BuildingType.flat

        assert (
            building.type
            == session.query(Building)
            .filter(Building.type == BuildingType.flat)
            .one()
            .type
        )

    def test_uuid(self):
        Base = Model(type_map=TypeMapper(types=[UUIDType]))

        class Node(Base):
            id: UUID = uuid.uuid4

        db = Database("sqlite:///:memory:")

        db.create(Node)
        session = db.session()

        node = Node()
        session.create(node)

        assert isinstance(node.id, UUID)

        fetched_node = session.query(Node).filter(Node.id == node.id).one()

        assert fetched_node
        assert node.id == fetched_node.id

    @pytest.mark.skipif(not is_pgvector_installed(), reason="PGVector is not installed")
    def test_pgvector(self):
        from pgvector.sqlalchemy import Vector

        Base = Model()

        class Document(Base):
            id: UUID = uuid.uuid4
            data: str
            embedding = mapped_column(Vector(3))

        db = Database("postgresql://postgres:@localhost:5432/sqlalchemy_orm")

        with db.engine.connect() as conn:
            conn.execute(text("CREATE EXTENSION IF NOT EXISTS vector"))
            conn.commit()

        session = db.session()

        db.create(Document)

        document_1 = Document(data="test_document_1", embedding=[1, 2, 3])
        document_2 = Document(data="test_document_2", embedding=[1, 1, 2])
        document_3 = Document(data="test_document_3", embedding=[1, 1, 1])
        session.create(document_1)
        session.create(document_2)
        session.create(document_3)

        documents = (
            session.query(Document)
            .order_by(Document.embedding.l2_distance([1, 1, 3]))
            .all()
        )
        assert [document_2, document_3, document_1] == documents
