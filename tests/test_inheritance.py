from sqlalchemy_orm.base.base import Model
from sqlalchemy_orm.database import Database


class TestInheritance:
    def test_abstract_base(self):
        class AbstractBase(Model()):
            id: str

            __abstract__ = True

        class Animal(AbstractBase):
            name: str
            age: int
            size: int

        db = Database("sqlite:///:memory:")

        db.create(Animal)

        primary_keys = Animal.primary_keys()

        assert 1 == len(primary_keys)
        assert primary_keys[0].key == "id"

    def test_auto_inheritance(self):
        Base = Model()

        class Building(Base):
            name: str
            number: int

        class RentedApartment(Building):
            landlord: str

        db = Database("sqlite:///:memory:")

        assert RentedApartment not in db

        db.create(RentedApartment)

        assert Building in db
        assert RentedApartment in db

        apartment = RentedApartment(
            name="58 City Road", number=58, landlord="Runway East"
        )

        session = db.session()
        session.create(apartment)

        rented_apartment_type = RentedApartment.__name__

        assert apartment._type == rented_apartment_type
        assert apartment == session.query(RentedApartment).one()
        assert (
            apartment
            == session.query(Building)
            .filter(Building._type == rented_apartment_type)
            .one()
        )

    def test_no_inheritance(self):
        Base = Model()

        class Building(Base):
            name: str
            number: int

            __inheritance__ = False

        class RentedApartment(Building):
            landlord: str

        db = Database("sqlite:///:memory:")

        assert Building not in db
        assert RentedApartment not in db

        db.create(RentedApartment)

        assert not Building._type
        assert not RentedApartment._type

    def test_multi_level_inheritance(self):
        Base = Model()

        class Animal(Base):
            name: str

        class Cat(Animal):
            color: str

        class Dog(Animal):
            age: str

        class BigCat(Cat):
            size: str

        dog = Dog(name="baxter", age="10")
        cat = Cat(name="chloe", color="yellow")
        big_cat = BigCat(name="poppy", color="yellow", size="big")
        db = Database("sqlite:///:memory:")

        db.create(Dog, BigCat)

        assert Animal in db and Cat in db and Dog in db and BigCat in db

        session = db.session()

        session.create(cat, dog, big_cat)

        assert dog == session.query(Dog).filter_by(name="baxter").one()
        assert dog == session.query(Animal).filter_by(name="baxter").one()
        assert cat == session.query(Cat).filter_by(name="chloe").one()
        assert big_cat == session.query(BigCat).filter_by(name="poppy").one()
        assert big_cat == session.query(Animal).filter_by(name="poppy").one()

        session.commit()

    def test_override(self):
        Base = Model()

        class Plant(Base):
            name: str = ""
            age: str = ""
            height: int = 0

        class CalculatedHeightMixin:
            height: int = 100

        class Tree(CalculatedHeightMixin, Plant):
            age: int = 0

        tree = Tree(name="willow", age=75)

        assert tree.height == 100

    def test_abstract_inheritance(self):
        Base = Model()

        class Building(Base):
            name: str
            number: int

            __abstract__ = True

        class RentedApartment(Building):
            landlord: str

        class Mansion(Building):
            staff: int

            __abstract__ = True

        class RobsMansion(Mansion):
            height: str

        db = Database("sqlite:///:memory:")

        db.create(RentedApartment, RobsMansion)

        assert Building not in db
        assert RentedApartment in db
        assert Mansion not in db
        assert RobsMansion in db

        class Furniture(Base):
            name: str

        class WoodFurniture(Furniture):
            wood_type: str

            __abstract__ = True

        class Chair(WoodFurniture):
            max_weight: int

        db.create(Chair)

        assert Chair in db
        assert Furniture in db
        assert WoodFurniture not in db
